<?php
$timestamp = date('Y-m-d H:i:s', time());
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    $pdo = new PDO("pgsql:host=127.0.0.1;dbname=crud", 'marathon', 'marathon2020');
    $stmt = $pdo->prepare('INSERT INTO article (name, description, created_at) VALUES (:name,:description,:created_at)');
    $stmt->bindValue(':name', $_POST['name']);
    $stmt->bindValue(':description', $_POST['description']);
    $stmt->bindValue(':created_at', $_POST['created_at']);
    $stmt->execute();
    header('Location: index.php');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create</title>
</head>
<body>
    <form method="post">
        <div class="form-row">
            <label>Назва</label>
            <input type="text" required name="name" placeholder="name">
        </div>
        <div class="form-row">
            <label>Опис</label>
            <input type="text" required name="description" placeholder="description">
        </div>
        <div class="form-row">
            <label>Дата створення</label>
            <input type="text" required name="created_at" placeholder="<?php echo $timestamp ?>">
        </div>
        <div class="form-row"><input type="submit"></div>
    </form>
</body>
</html>